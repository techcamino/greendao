package com.techcamino.greendao;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import com.techcamino.greendao.db.DaoMaster;
import com.techcamino.greendao.db.DaoSession;


/**
 * Created by Thinkpad on 04-02-2017.
 */

public class MyApplication extends Application {

    private DaoSession daoSession;
    private  final String DB_NAME ="pos-db" ;

    @Override
    public void onCreate() {
        super.onCreate();
        // Required initialization logic here!
        DaoMaster.DevOpenHelper masterHelper = new DaoMaster.DevOpenHelper(this, DB_NAME, null); //create database db file if not exist
        SQLiteDatabase dbs = masterHelper.getWritableDatabase();  //get the created database db file
        DaoMaster master = new DaoMaster(dbs);//create masterDao
        daoSession = master.newSession(); //Creates Session session
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }
}