package com.techcamino.greendao;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import com.techcamino.greendao.db.DaoSession;
import com.techcamino.greendao.db.ProductMaster;
import com.techcamino.greendao.db.ProductMasterDao;

public class MainActivity extends AppCompatActivity {

    private Context context = this;
    private ProductMaster productMaster;
    private ProductMasterDao productMasterDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DaoSession daoSession = ((MyApplication)getApplication()).getDaoSession();
        productMasterDao = daoSession.getProductMasterDao();
        productMaster = new ProductMaster();
        productMaster.setName("Avinash");
        Long id = productMasterDao.insertOrReplace(productMaster);
        Log.d("Database",id+"Hello");
    }
}
