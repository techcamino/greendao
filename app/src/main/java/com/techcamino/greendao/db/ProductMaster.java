package com.techcamino.greendao.db;

import org.greenrobot.greendao.annotation.*;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END

/**
 * Entity mapped to table "PRODUCT_MASTER".
 */
@Entity
public class ProductMaster {

    @Id(autoincrement = true)
    private Long id;

    @NotNull
    private String name;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    @Generated
    public ProductMaster() {
    }

    public ProductMaster(Long id) {
        this.id = id;
    }

    @Generated
    public ProductMaster(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public String getName() {
        return name;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setName(@NotNull String name) {
        this.name = name;
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
