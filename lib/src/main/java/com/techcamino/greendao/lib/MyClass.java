package com.techcamino.greendao.lib;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Schema;

public class MyClass {

    public static void main(String[] args) throws Exception {
//place where db folder will be created inside the project folder
        Schema schema = new Schema(1, "com.techcamino.greendao.db"); // this is the package name where generated code will go into
        schema.enableKeepSectionsByDefault();
//Entity i.e. Class to be stored in the database // ie table LOG
        addTables(schema);
//  ./app/src/main/java/   ----   com/codekrypt/greendao/db is the full path
        new DaoGenerator().generateAll(schema, "./app/src/main/java");
    }

    private static void addTables(final Schema schema) {
        Entity product = addProduct(schema);
    }

    private static Entity addProduct(final Schema schema) {
        Entity product = schema.addEntity("ProductMaster"); //productmaster and productmasterdao
        product.addIdProperty().primaryKey().autoincrement();
        product.addStringProperty("name").notNull();
        return product;
    }

}
